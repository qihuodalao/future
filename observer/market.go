package main

import (
	"github.com/streadway/amqp"
	"log"
)

func market() {
	connection, err := amqp.Dial("amqp://guest:guest@192.168.0.123/")
	if err != nil {
		log.Fatalln(err)
	}

	channel, err := connection.Channel()
	if err != nil {
		log.Fatalln(err)
	}

	deliveries, err := channel.Consume("market", "observer", false, false, false, false, nil)
	if err != nil {
		log.Fatalln(err)
	}

	for delivery := range deliveries {
		log.Println(string(delivery.Body))

		err = delivery.Ack(false)
		if err != nil {
			log.Fatalln(err)
		}
	}
}
