package main

import (
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"log"
)

func web() {
	engine := gin.New()

	engine.LoadHTMLFiles("home.html")
	home(engine)

	upgrader := websocket.Upgrader{}
	ws(engine, upgrader)

	err := engine.Run()
	if err != nil {
		log.Fatalln(err)
	}
}

func home(engine *gin.Engine) {
	engine.GET("/", func(context *gin.Context) {
		context.HTML(200, "home.html", nil)
	})
}

func ws(engine *gin.Engine, upgrader websocket.Upgrader) {
	engine.GET("/ws", func(context *gin.Context) {
		if !context.IsWebsocket() {
			context.AbortWithStatus(501)
			return
		}

		conn, err := upgrader.Upgrade(context.Writer, context.Request, nil)
		if err != nil {
			log.Fatalln(err)
		}

		for {
			messageType, p, err := conn.ReadMessage()
			if err != nil {
				log.Fatalln(err)
			}

			err = conn.WriteMessage(messageType, p)
			if err != nil {
				log.Fatalln(err)
			}
		}
	})
}
