//
// Created by john on 2019/7/31.
//

#ifndef MARKET_MARKET_H
#define MARKET_MARKET_H


#include <iostream>
#include "ThostFtdcMdApi.h"
#include "MessageQueue.h"
#include "json.hpp"

class Market : public CThostFtdcMdSpi {

public:
    void start(const std::string &address, char **instruments, int instrumentCount, const std::string &MQAddress);

private:
    CThostFtdcMdApi *_api{};

    int _requestID = 0;

    char *_address = nullptr;
    char **_instruments = nullptr;
    int _instrumentsCount = 0;

    AMQP::TcpChannel *_channel{};

    void connect();

    void OnFrontConnected() override;

    void OnFrontDisconnected(int nReason) override;

    void OnHeartBeatWarning(int nTimeLapse) override;

    void login();

    void
    OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin, CThostFtdcRspInfoField *pRspInfo, int, bool) override;

    void subscribe();

    void
    OnRspSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int,
                       bool) override;

    void OnRtnDepthMarketData(CThostFtdcDepthMarketDataField *pDepthMarketData) override;

    void OnRspError(CThostFtdcRspInfoField *pRspInfo, int, bool) override;

};


#endif //MARKET_MARKET_H
