//
// Created by john on 2019/8/10.
//

#include "MessageQueue.h"

void MessageQueue::onConnected(AMQP::TcpConnection *) {
    std::cout << "--------MQ--------" << std::endl;
    std::cout << "onConnected" << std::endl;
}

void MessageQueue::onReady(AMQP::TcpConnection *) {
    std::cout << "--------MQ--------" << std::endl;
    std::cout << "onReady" << std::endl;
}

uint16_t MessageQueue::onNegotiate(AMQP::TcpConnection *, uint16_t) {
    return 10;
}

void MessageQueue::onHeartbeat(AMQP::TcpConnection *connection) {
    connection->heartbeat();
}

void MessageQueue::onLost(AMQP::TcpConnection *) {
    std::cout << "--------MQ--------" << std::endl;
    std::cout << "onLost" << std::endl;
}

void MessageQueue::onError(AMQP::TcpConnection *, const char *message) {
    std::cout << "--------MQ--------" << std::endl;
    std::cout << "onError" << std::endl;
    std::cout << message << std::endl;
}
