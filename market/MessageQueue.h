//
// Created by john on 2019/8/10.
//

#ifndef MARKET_MESSAGEQUEUE_H
#define MARKET_MESSAGEQUEUE_H


#include "amqpcpp/libuv.h"

class MessageQueue : public AMQP::LibUvHandler {

public:
    explicit MessageQueue(uv_loop_t *loop) : AMQP::LibUvHandler(loop) {}

private:
    void onConnected(AMQP::TcpConnection *) override;

    void onReady(AMQP::TcpConnection *) override;

    uint16_t onNegotiate(AMQP::TcpConnection *, uint16_t) override;

    void onHeartbeat(AMQP::TcpConnection *connection) override;

    void onLost(AMQP::TcpConnection *) override;

    void onError(AMQP::TcpConnection *, const char *message) override;

};


#endif //MARKET_MESSAGEQUEUE_H
