//
// Created by john on 2019/7/31.
//

#include "Market.h"

void Market::start(const std::string &address, char **instruments, int instrumentsCount, const std::string &MQAddress) {
    this->_address = const_cast<char *>(address.c_str());
    this->_instruments = instruments;
    this->_instrumentsCount = instrumentsCount;

    uv_loop_t *loop = uv_default_loop();

    MessageQueue queue(loop);
    AMQP::TcpConnection connection(&queue, AMQP::Address(MQAddress));
    AMQP::TcpChannel channel = AMQP::TcpChannel(&connection);
    this->_channel = &channel;

    channel.declareExchange("market", AMQP::ExchangeType::fanout, AMQP::durable);
    channel.declareQueue("market", AMQP::durable);
    channel.bindQueue("market", "market", "");

    channel.onReady([this]() {
        this->connect();
    });

    uv_run(loop, UV_RUN_DEFAULT);
}

/**
 * connect
 */
void Market::connect() {
    _api = CThostFtdcMdApi::CreateFtdcMdApi();
    _api->RegisterSpi(this);
    _api->RegisterFront(_address);
    _api->Init();
}

void Market::OnFrontConnected() {
    std::cout << "--------CTP--------" << std::endl;
    std::cout << "OnFrontConnected" << std::endl;

    this->login();
}

void Market::OnFrontDisconnected(int nReason) {
    std::cout << "--------CTP--------" << std::endl;
    std::cout << "OnFrontDisconnected" << std::endl;

    if (nReason == 0x1001) {
        std::cout << "网络读失败" << std::endl;
    } else if (nReason == 0x1002) {
        std::cout << "网络写失败" << std::endl;
    } else if (nReason == 0x2001) {
        std::cout << "接收心跳超时" << std::endl;
    } else if (nReason == 0x2002) {
        std::cout << "发送心跳失败" << std::endl;
    } else if (nReason == 0x2003) {
        std::cout << "收到错误报文" << std::endl;
    } else {
        std::cout << nReason << std::endl;
    }
}

void Market::OnHeartBeatWarning(int nTimeLapse) {
    std::cout << "--------CTP--------" << std::endl;
    std::cout << "OnHeartBeatWarning" << std::endl;
    std::cout << nTimeLapse << std::endl;
}

/**
 * login
 */
void Market::login() {
    CThostFtdcReqUserLoginField field = {};
    int login = _api->ReqUserLogin(&field, _requestID++);

    if (login != 0) {
        std::cout << "--------CTP--------" << std::endl;
        std::cout << "login" << std::endl;

        if (login == -1) {
            std::cout << "网络连接失败" << std::endl;
        } else if (login == -2) {
            std::cout << "未处理请求超过许可数" << std::endl;
        } else if (login == -3) {
            std::cout << "每秒发送请求数超过许可数" << std::endl;
        } else {
            std::cout << login << std::endl;
        }
    }
}

void Market::OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin, CThostFtdcRspInfoField *pRspInfo, int, bool) {
    std::cout << "--------CTP--------" << std::endl;
    std::cout << "OnRspUserLogin" << std::endl;
    std::cout << pRspUserLogin->TradingDay << std::endl;
    std::cout << pRspInfo->ErrorMsg << std::endl;

    this->subscribe();
}

/**
 * subscribe
 */
void Market::subscribe() {
    int subscribe = _api->SubscribeMarketData(_instruments, _instrumentsCount);

    if (subscribe != 0) {
        std::cout << "--------CTP--------" << std::endl;
        std::cout << "subscribe" << std::endl;

        if (subscribe == -1) {
            std::cout << "网络连接失败" << std::endl;
        } else if (subscribe == -2) {
            std::cout << "未处理请求超过许可数" << std::endl;
        } else if (subscribe == -3) {
            std::cout << "每秒发送请求数超过许可数" << std::endl;
        } else {
            std::cout << subscribe << std::endl;
        }
    }
}

void
Market::OnRspSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo,
                           int, bool) {
    std::cout << "--------CTP--------" << std::endl;
    std::cout << "OnRspSubMarketData" << std::endl;
    std::cout << pSpecificInstrument->InstrumentID << std::endl;
    std::cout << pRspInfo->ErrorMsg << std::endl;
}

void Market::OnRtnDepthMarketData(CThostFtdcDepthMarketDataField *pDepthMarketData) {
    nlohmann::json json;
    json["instrument"] = pDepthMarketData->InstrumentID;
    json["price"] = pDepthMarketData->LastPrice;
    json["date"] = pDepthMarketData->TradingDay;
    json["time"] = pDepthMarketData->UpdateTime;
    json["millisec"] = pDepthMarketData->UpdateMillisec;

    std::string body = json.dump();
    AMQP::Envelope envelope(body.c_str(), body.size());
    envelope.setContentType("application/json");
    _channel->publish("market", "", envelope);
}

void Market::OnRspError(CThostFtdcRspInfoField *pRspInfo, int, bool) {
    std::cout << "--------CTP--------" << std::endl;
    std::cout << "OnRspError" << std::endl;
    std::cout << pRspInfo->ErrorMsg << std::endl;
}
