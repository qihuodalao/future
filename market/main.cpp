#include "Market.h"

int main() {
    std::string address = "tcp://61.186.254.135:42213"; // pro
//    std::string address = "tcp://180.168.146.187:10131"; // test

    std::string MQAddress = "amqp://guest:guest@192.168.0.168/"; // pro
//    std::string MQAddress = "amqp://guest:guest@192.168.0.123/"; // test

    int instrumentsCount = 23;
    char **instruments = new char *[instrumentsCount];

    std::string ag1912 = "ag1912"; // 白银 上海
    instruments[0] = const_cast<char *>(ag1912.c_str());

    std::string rb1910 = "rb2001"; // 螺纹 上海
    instruments[1] = const_cast<char *>(rb1910.c_str());

    std::string hc1910 = "hc2001"; // 热卷 上海
    instruments[2] = const_cast<char *>(hc1910.c_str());

    std::string sp2001 = "sp2001"; // 纸浆 上海
    instruments[3] = const_cast<char *>(sp2001.c_str());

    std::string fu2001 = "fu2001"; // 燃油 上海
    instruments[4] = const_cast<char *>(fu2001.c_str());

    std::string bu1912 = "bu1912"; // 沥青 上海
    instruments[5] = const_cast<char *>(bu1912.c_str());

    std::string FG001 = "FG001"; // 玻璃 郑州
    instruments[6] = const_cast<char *>(FG001.c_str());

    std::string TA001 = "TA001"; // PTA 郑州
    instruments[7] = const_cast<char *>(TA001.c_str());

    std::string MA001 = "MA001"; // 甲醇 郑州
    instruments[8] = const_cast<char *>(MA001.c_str());

    std::string OI001 = "OI001"; // 菜油 郑州
    instruments[9] = const_cast<char *>(OI001.c_str());

    std::string RM001 = "RM001"; // 菜粕 郑州
    instruments[10] = const_cast<char *>(RM001.c_str());

    std::string SR001 = "SR001"; // 白糖 郑州
    instruments[11] = const_cast<char *>(SR001.c_str());

    std::string l2001 = "l2001"; // 塑料 大连
    instruments[12] = const_cast<char *>(l2001.c_str());

    std::string v2001 = "v2001"; // PVC 大连
    instruments[13] = const_cast<char *>(v2001.c_str());

    std::string pp2001 = "pp2001"; // PP 大连
    instruments[14] = const_cast<char *>(pp2001.c_str());

    std::string eg2001 = "eg2001"; // 乙二醇 大连
    instruments[15] = const_cast<char *>(eg2001.c_str());

    std::string c2001 = "c2001"; // 玉米 大连
    instruments[16] = const_cast<char *>(c2001.c_str());

    std::string cs2001 = "cs2001"; // 淀粉 大连
    instruments[17] = const_cast<char *>(cs2001.c_str());

    std::string a2001 = "a2001"; // 豆一 大连
    instruments[18] = const_cast<char *>(a2001.c_str());

    std::string b1910 = "b1910"; // 豆二 大连
    instruments[19] = const_cast<char *>(b1910.c_str());

    std::string m2001 = "m2001"; // 豆粕 大连
    instruments[20] = const_cast<char *>(m2001.c_str());

    std::string y2001 = "y2001"; // 豆油 大连
    instruments[21] = const_cast<char *>(y2001.c_str());

    std::string p2001 = "p2001"; // 棕桐 大连
    instruments[22] = const_cast<char *>(p2001.c_str());

    auto *market = new Market();
    market->start(address, instruments, instrumentsCount, MQAddress);
}
