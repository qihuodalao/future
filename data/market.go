package main

import (
	"encoding/json"
	"fmt"
	"github.com/streadway/amqp"
	"log"
	"time"
)

type MarketBody struct {
	Instrument string
	Price      float64
	Date       string
	Time       string
	Millisec   int
}

type MarketBodyWithTime struct {
	MarketBody
	DateTime time.Time
}

var marketBodys map[string]MarketBodyWithTime

func Market(amqpAddress string, postgresAddress string) {
	connection, err := amqp.Dial(amqpAddress)
	if err != nil {
		log.Fatalln(err)
	} else {
		log.Println("--------Market-MQ--------")
		log.Println("connection")
	}

	channel, err := connection.Channel()
	if err != nil {
		log.Fatalln(err)
	}

	queueDeclare(channel)
	startTask(amqpAddress, postgresAddress)

	deliveries, err := channel.Consume("market", "data", false, false, false, false, nil)
	if err != nil {
		log.Fatalln(err)
	} else {
		log.Println("--------Market-MQ--------")
		log.Println("delivery")
	}

	marketBodys = make(map[string]MarketBodyWithTime)

	for delivery := range deliveries {
		var body MarketBody;
		err = json.Unmarshal(delivery.Body, &body)
		if err != nil {
			log.Fatalln(err)
		}

		closePrice(body, channel)

		err = delivery.Ack(false)
		if err != nil {
			log.Fatalln(err)
		}
	}
}

func closePrice(body MarketBody, channel *amqp.Channel) {
	millisec := fmt.Sprintf("%03d", body.Millisec)
	dateTime, err := time.ParseInLocation("20060102 15:04:05.000", body.Date+" "+body.Time+"."+millisec, time.Local)
	if err != nil {
		log.Fatalln(err)
	}

	bodyWithTime, isExist := marketBodys[body.Instrument]
	if !isExist {
		marketBodys[body.Instrument] = MarketBodyWithTime{body, dateTime}
	}

	if isExist && bodyWithTime.DateTime.Minute() != dateTime.Minute() {
		bodyJson, err := json.Marshal(bodyWithTime)
		if err != nil {
			log.Fatalln(err)
		}

		err = channel.Publish("data", "", false, false, amqp.Publishing{
			ContentType: "application/json",
			Body:        bodyJson,
		})
		if err != nil {
			log.Fatalln(err)
		}

		marketBodys[body.Instrument] = MarketBodyWithTime{body, dateTime}
	}
}

func queueDeclare(channel *amqp.Channel) {
	err := channel.ExchangeDeclare("data", amqp.ExchangeFanout, true, false, false, false, nil)
	if err != nil {
		log.Fatalln(err)
	}

	_, err = channel.QueueDeclare("data-postgres", true, false, false, false, nil)
	if err != nil {
		log.Fatalln(err)
	}
	err = channel.QueueBind("data-postgres", "", "data", false, nil)
	if err != nil {
		log.Fatalln(err)
	}

	_, err = channel.QueueDeclare("data-ma", true, false, false, false, nil)
	if err != nil {
		log.Fatalln(err)
	}
	err = channel.QueueBind("data-ma", "", "data", false, nil)
	if err != nil {
		log.Fatalln(err)
	}

	_, err = channel.QueueDeclare("data-macd", true, false, false, false, nil)
	if err != nil {
		log.Fatalln(err)
	}
	err = channel.QueueBind("data-macd", "", "data", false, nil)
	if err != nil {
		log.Fatalln(err)
	}
}

func startTask(amqpAddress string, postgresAddress string) {
	go Postgres(amqpAddress, postgresAddress)
	//go Ma(amqpAddress)
	//go Macd(amqpAddress)
}
