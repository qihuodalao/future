package main

import (
	"encoding/json"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/streadway/amqp"
	"log"
)

type MarketModel struct {
	gorm.Model
	MarketBodyWithTime
}

func (MarketModel) TableName() string {
	return "markets"
}

func Postgres(amqpAddress string, postgresAddress string) {
	connection, err := amqp.Dial(amqpAddress)
	if err != nil {
		log.Fatalln(err)
	} else {
		log.Println("--------Postgres-MQ--------")
		log.Println("connection")
	}

	channel, err := connection.Channel()
	if err != nil {
		log.Fatalln(err)
	}

	deliveries, err := channel.Consume("data-postgres", "postgres", false, false, false, false, nil)
	if err != nil {
		log.Fatalln(err)
	} else {
		log.Println("--------Postgres-MQ--------")
		log.Println("delivery")
	}

	db, err := gorm.Open("postgres", postgresAddress)
	if err != nil {
		log.Fatalln(err)
	} else {
		log.Println("--------Postgres-DB--------")
		log.Println("open")
	}
	db.AutoMigrate(&MarketModel{})

	for delivery := range deliveries {
		var body MarketBodyWithTime;
		err = json.Unmarshal(delivery.Body, &body)
		if err != nil {
			log.Fatalln(err)
		}

		db.Create(&MarketModel{MarketBodyWithTime: body})

		err = delivery.Ack(false)
		if err != nil {
			log.Fatalln(err)
		}
	}
}
