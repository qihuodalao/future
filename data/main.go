package main

func main() {
	amqpAddress := "amqp://guest:guest@192.168.0.168/" // pro
	//amqpAddress := "amqp://guest:guest@192.168.0.123/" // test

	postgresAddress := "host=192.168.0.168 port=5432 dbname=future user=postgres password=postgres sslmode=disable" // pro
	//postgresAddress := "host=192.168.0.123 port=5432 dbname=future user=postgres password=postgres sslmode=disable" // test

	Market(amqpAddress, postgresAddress)
}
