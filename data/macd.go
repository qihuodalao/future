package main

import (
	"encoding/json"
	"github.com/streadway/amqp"
	"log"
)

func Macd(amqpAddress string) {
	connection, err := amqp.Dial(amqpAddress)
	if err != nil {
		log.Fatalln(err)
	} else {
		log.Println("--------Macd-MQ--------")
		log.Println("connection")
	}

	channel, err := connection.Channel()
	if err != nil {
		log.Fatalln(err)
	}

	deliveries, err := channel.Consume("data-macd", "macd", false, false, false, false, nil)
	if err != nil {
		log.Fatalln(err)
	} else {
		log.Println("--------Macd-MQ--------")
		log.Println("delivery")
	}

	for delivery := range deliveries {
		var body MarketBodyWithTime;
		err = json.Unmarshal(delivery.Body, &body)
		if err != nil {
			log.Fatalln(err)
		}

		err = delivery.Ack(false)
		if err != nil {
			log.Fatalln(err)
		}
	}
}
