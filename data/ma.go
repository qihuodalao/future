package main

import (
	"encoding/json"
	"github.com/streadway/amqp"
	"log"
)

var ma5Bodys []MarketBodyWithTime

func Ma(amqpAddress string) {
	connection, err := amqp.Dial(amqpAddress)
	if err != nil {
		log.Fatalln(err)
	} else {
		log.Println("--------Ma-MQ--------")
		log.Println("connection")
	}

	channel, err := connection.Channel()
	if err != nil {
		log.Fatalln(err)
	}

	deliveries, err := channel.Consume("data-ma", "ma", false, false, false, false, nil)
	if err != nil {
		log.Fatalln(err)
	} else {
		log.Println("--------Ma-MQ--------")
		log.Println("delivery")
	}

	for delivery := range deliveries {
		var body MarketBodyWithTime;
		err = json.Unmarshal(delivery.Body, &body)
		if err != nil {
			log.Fatalln(err)
		}

		ma5(body, channel)

		err = delivery.Ack(false)
		if err != nil {
			log.Fatalln(err)
		}
	}
}

func ma5(body MarketBodyWithTime, channel *amqp.Channel) {

	if len(ma5Bodys) >= 5 {
		ma5Bodys = ma5Bodys[1:]
	}

	ma5Bodys = append(ma5Bodys, body)
}
